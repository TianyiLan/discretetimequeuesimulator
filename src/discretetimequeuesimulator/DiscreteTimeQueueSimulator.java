/*
 * CSE 346 Project #2 Discrete Time Queue
 * Professor: T.G.Robertazzi
 * Programming language: Java
 * Stony Brook University
 * Spring 2016 
 */
package discretetimequeuesimulator;

import static discretetimequeuesimulator.DTQSimulation.LOWER_BOUNDRY_TIME_SLOT;
import static discretetimequeuesimulator.DTQSimulation.UPPER_BOUNDRY_TIME_SLOT;
import discretetimequeuesimulator.DTQSimulation.simulationDataEntry;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * This is a program that simulates a descrete time queueing system, which can
 * only store 5 packets. At each time slot, it randomly processes a packet in
 * the server, and the serviced packet will depart, then the packets in the
 * buffer(if any) will be shifted towards the output. Afterwards, it randomly
 * generate a new packet for input; if the queuesing system is full, the packet
 * will be dropped, otherwise, it will be stored and queued. Processing is on a
 * FIFO basis, and a departure occurs before an arrival. All probabilities are
 * given.
 *
 * @author Tianyi Lan
 */
public class DiscreteTimeQueueSimulator extends Application {

    @Override
    public void start(Stage stage) {
        //for the program, we need 1 simulation and 2 graphs
        DTQSimulation simu = new DTQSimulation();
        //graph of "Average Throughput vs. Probability"
        Simple2DGraph g1 = new Simple2DGraph();
        //graph of "Average Delay vs. Probability"
        Simple2DGraph g2 = new Simple2DGraph();

        //set up graphs
        g1.setGraphTitle("Average Throughput vs. Probability ");
        g1.setXLabel("Probability");
        g1.setYLabel("Average Throughput");
        g1.setSeriesName("Time Slot = " + simu.getSlot());

        g2.setGraphTitle("Average Delay vs. Probability ");
        g2.setXLabel("Probability");
        g2.setYLabel("Average Delay");
        g2.setSeriesName("Time Slot = " + simu.getSlot());

        //helper inner class for populating data everytime we run simulation
        class dataPopulator {

            //populate both graphs with simulation data
            public void populate() {
                for (int i = 0; i < simu.getSimuData().size(); i++) {
                    g1.inputData(simu.getSimuData().get(i).getProbability(), simu.getSimuData().get(i).getAveThroughput());
                    g2.inputData(simu.getSimuData().get(i).getProbability(), simu.getSimuData().get(i).getAveDelay());
                }
            }
        }
        dataPopulator dp = new dataPopulator();

        //main GUI, we place graphs on left, data on right
        HBox gui = new HBox();
        gui.setAlignment(Pos.CENTER);

        //start page, when program starts, there will be a start page
        VBox home = new VBox();
        home.setAlignment(Pos.CENTER);
        Label title = new Label("Discrete Time Queue Simulator");
        title.setFont(Font.font("Cambria", 80));
        title.setTextFill(Color.RED);
        Label author = new Label("Designed by Tianyi Lan");
        author.setFont(Font.font("Cambria", 20));
        author.setTextFill(Color.RED);
        DropShadow ds = new DropShadow();
        ds.setOffsetX(5);
        ds.setOffsetY(5);
        ds.setColor(Color.GRAY);
        title.setEffect(ds);
        author.setEffect(ds);
        home.getChildren().addAll(title, author);

        //left graph layer, place the graph in the center, a toolbar in the bottom
        BorderPane graphLayer = new BorderPane();

        //toolbar
        HBox toolbar = new HBox();
        toolbar.setAlignment(Pos.CENTER);
        toolbar.setPadding(new Insets(10, 5, 10, 5));
        toolbar.setSpacing(20);

        //construct all buttons
        Button start = new Button("Start Simulation");
        Button change = new Button("Switch Graph");
        Button less = new Button("Less Time Slot");
        Button more = new Button("More Time Slot");
        Button rerun = new Button("Rerun");
        Button help = new Button("?");
        start.setStyle("-fx-font: 18 arial; -fx-base: #c9e3d8;");
        change.setStyle("-fx-font: 14 arial; -fx-base: #c9e3d8;");
        less.setStyle("-fx-font: 14 arial; -fx-base: #c9e3d8;");
        more.setStyle("-fx-font: 14 arial; -fx-base: #c9e3d8;");
        rerun.setStyle("-fx-font: 14 arial; -fx-base: #c9e3d8;");
        help.setStyle("-fx-font: 14 arial; -fx-base: #c9e3d8;");
        
        //checkbox for points
        CheckBox show = new CheckBox("Entries");
        show.setStyle("-fx-font-size: 14;  -fx-text-fill: #c9e3d8;");
        
        //construct text field for time slot
        TextField numSlot = new TextField();
        numSlot.setPromptText("Number of time slots");
        
        //right data layer
        VBox dataLayer = new VBox();
        //table view for displaying simulation data
        TableView<simulationDataEntry> table = new TableView<>();
        //bind probability to column 1
        TableColumn<simulationDataEntry, Number> pCol = new TableColumn<>("Probability");
        pCol.setMinWidth(50);
        pCol.setCellValueFactory(cellData -> cellData.getValue().pProperty());
        //bind average throughput to column 2
        TableColumn<simulationDataEntry, Number> aveTCol = new TableColumn<>("Ave Throughput");
        aveTCol.setMinWidth(110);
        aveTCol.setCellValueFactory(cellData -> cellData.getValue().aveTProperty());
        //bind average delay to column 3
        TableColumn<simulationDataEntry, Number> aveDCol = new TableColumn<>("Ave Delay");
        aveDCol.setMinWidth(80);
        aveDCol.setCellValueFactory(cellData -> cellData.getValue().aveDProperty());
        //insert data to table
        table.setItems(simu.getSimuData());
        table.getColumns().addAll(pCol, aveTCol, aveDCol);
        table.setEditable(false);
        //add a title for the table
        Label tableTitle = new Label("Simulation Data");
        tableTitle.setFont(new Font("Arial", 18));
        dataLayer.setSpacing(5);
        dataLayer.setPadding(new Insets(10, 10, 10, 0));
        VBox.setVgrow(table, Priority.ALWAYS);
        dataLayer.getChildren().addAll(tableTitle, table);

        //assemble components
        //initially, a start page and only one button are visiable
        toolbar.getChildren().add(start);
        graphLayer.setCenter(home);
        graphLayer.setBottom(toolbar);
        gui.getChildren().add(graphLayer);
        HBox.setHgrow(graphLayer, Priority.ALWAYS);

        //default size of window, note that it is resizable
        Scene scene = new Scene(gui);
        stage.setTitle("Discrete Time Queue Simulator");
        stage.setScene(scene);
        stage.setWidth(1200);
        stage.setHeight(700);
        stage.setResizable(true);
        stage.show();

        //define functionality for all bottons
        //start button, by clicking which will run the simulation with default slot(10000) 
        //replace start page with graph, and all other buttons with start button
        //place data layer on the right
        //note that the default graph is "Average Throughput vs. Probability" 
        start.setOnAction((ActionEvent e) -> {
            //start simulation
            simu.simulate();
            //feed data to graphs
            dp.populate();
            //add all buttons
            toolbar.getChildren().clear();
            HBox pane = new HBox(help);
            pane.setAlignment(Pos.CENTER_RIGHT);
            toolbar.getChildren().addAll(less, change, more, numSlot, rerun, show, pane);
            HBox.setHgrow(pane, Priority.ALWAYS);
            toolbar.setStyle("-fx-background-color: #305098;");

            //plot graph
            g1.plot(graphLayer);
            g1.showPoints(false);
            g2.showPoints(false);
            //place data layer on the right
            gui.getChildren().add(dataLayer);
        });

        //change button, by clicking which will switch between 2 graphs
        change.setOnAction((ActionEvent e) -> {
            //if current graph is "throughput", switch to "delay"
            if (graphLayer.getCenter() == g1.getGraph()) {
                g2.plot(graphLayer);
                //if current graph is "delay", switch to "throughput"   
            } else if (graphLayer.getCenter() == g2.getGraph()) {
                g1.plot(graphLayer);
            }
        });

        //less button, by clicking which will rerun the simulation with less time slots 
        //the lower boundry of slot is 100, when hit, this button will be disabled 
        less.setOnAction((ActionEvent e) -> {
            //if current slot is 100000(upper boundry), clicking less enables more
            if (more.isDisable()) {
                more.setDisable(false);
            }
            //decrease time slots
            simu.lessSlot();
            //if hit the lower boundry, disable less button
            if (simu.hitLowerBoundry()) {
                less.setDisable(true);
            }
            //rerun the simulation
            simu.simulate();
            //clear series data, update series names
            g1.clearSeries();
            g1.setSeriesName("Time Slot = " + simu.getSlot());
            g2.clearSeries();
            g2.setSeriesName("Time Slot = " + simu.getSlot());
            //feed data to graphs
            dp.populate();
            //replot the current graph
            if (graphLayer.getCenter() == g1.getGraph()) {
                g1.plot(graphLayer);
            } else if (graphLayer.getCenter() == g2.getGraph()) {
                g2.plot(graphLayer);
            }
        });

        //more button, by clicking which will rerun the simulation with more time slots 
        //the upper boundry of slot is 100000, when hit, this button will be disabled 
        more.setOnAction((ActionEvent e) -> {
            //if current slot is 100(lower boundry), clicking more enables less
            if (less.isDisable()) {
                less.setDisable(false);
            }
            //increase time slots
            simu.moreSlot();
            //if hit the upper boundry, disable more button
            if (simu.hitUpperBoundry()) {
                more.setDisable(true);
            }
            //rerun the simulation
            simu.simulate();
            //clear series data, update series names
            g1.clearSeries();
            g1.setSeriesName("Time Slot = " + simu.getSlot());
            g2.clearSeries();
            g2.setSeriesName("Time Slot = " + simu.getSlot());
            //feed data to graphs
            dp.populate();
            //replot the current graph
            if (graphLayer.getCenter() == g1.getGraph()) {
                g1.plot(graphLayer);
            } else if (graphLayer.getCenter() == g2.getGraph()) {
                g2.plot(graphLayer);
            }
        });

        //rerun button, by clicking which will rerun the simulation with the time slots entered
        rerun.setOnAction((ActionEvent e) -> {
            int slot;
            //error message dialog
            Alert errorMSG = new Alert(AlertType.ERROR);
            errorMSG.setTitle("Invalid Input");
            errorMSG.setHeaderText(null);
            errorMSG.setContentText
        ("Please enter an integer between " +LOWER_BOUNDRY_TIME_SLOT + " and "+ UPPER_BOUNDRY_TIME_SLOT+"." );

            //if empty
            if (numSlot.getText() == null || numSlot.getText().isEmpty()) {
                errorMSG.showAndWait();
                numSlot.clear();
                return;
            } //if not numeric input
            else if (!numSlot.getText().matches("[0-9]*")) {
                errorMSG.showAndWait();
                numSlot.clear();
                return;
            } //if input out of boundry
            else if (Integer.valueOf(numSlot.getText()) < LOWER_BOUNDRY_TIME_SLOT  || Integer.valueOf(numSlot.getText()) > UPPER_BOUNDRY_TIME_SLOT ) {
                errorMSG.showAndWait();
                numSlot.clear();
                return;
            } //valid input
            else {
                slot = Integer.valueOf(numSlot.getText());
            }

            //set the time slot for the simulation
            simu.setSlot(slot);

            //adjust button disabilities
            if (!simu.hitUpperBoundry() && more.isDisable()) {
                more.setDisable(false);
            }
            if (simu.hitUpperBoundry()) {
                more.setDisable(true);
            }
            if (!simu.hitLowerBoundry() && less.isDisable()) {
                less.setDisable(false);
            }
            if (simu.hitLowerBoundry()) {
                less.setDisable(true);
            }

            //rerun the simulation
            simu.simulate();
            //clear series data, update series names
            g1.clearSeries();
            g1.setSeriesName("Time Slot = " + simu.getSlot());
            g2.clearSeries();
            g2.setSeriesName("Time Slot = " + simu.getSlot());
            //feed data to graphs
            dp.populate();
            //replot the current graph
            if (graphLayer.getCenter() == g1.getGraph()) {
                g1.plot(graphLayer);
            } else if (graphLayer.getCenter() == g2.getGraph()) {
                g2.plot(graphLayer);
            }
        });

        //help button, by clicking which will display help message
        help.setOnAction((ActionEvent e) -> {
            //help message dialog
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Help Message");
            alert.setHeaderText(null);
            alert.setContentText("This application simulates a descrete time "
                    + "queueing system with the time slots from "+LOWER_BOUNDRY_TIME_SLOT +" to "+UPPER_BOUNDRY_TIME_SLOT +".");
            alert.showAndWait();
        });
        
        //show points check box
        show.setOnAction(e->{
            //check to show points
            if(show.isSelected()){
                g1.showPoints(true);
                g2.showPoints(true);
            }//uncheck to hide points
            else{
                g1.showPoints(false);
                g2.showPoints(false);
            }
        });
        
    }

    public static void main(String[] args) {
        launch(args);
    }
}
