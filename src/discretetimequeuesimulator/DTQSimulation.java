/*
 * CSE 346 Project #2 Discrete Time Queue
 * Professor: T.G.Robertazzi
 * Programming language: Java
 * Stony Brook University
 * Spring 2016 
 */
package discretetimequeuesimulator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * This class performs the simulation and stores the relevant data. We will use
 * an observatblelist to store the data, since the GUI can be easily updated as
 * the data changes.
 *
 * @author Tianyi Lan
 */
public class DTQSimulation {

    //define a class to store results, convenient for pulling out data
    public class simulationDataEntry {

        private final DoubleProperty probability;
        private final DoubleProperty aveThroughput;
        private final DoubleProperty aveDelay;

        //constructor
        public simulationDataEntry(double p, double aveThruput, double aveDelay) {
            this.probability = new SimpleDoubleProperty(p);
            this.aveThroughput = new SimpleDoubleProperty(aveThruput);
            this.aveDelay = new SimpleDoubleProperty(aveDelay);
        }

        //some accessor methods
        public double getProbability() {
            return probability.get();
        }

        public double getAveThroughput() {
            return aveThroughput.get();
        }

        public double getAveDelay() {
            return aveDelay.get();
        }

        public DoubleProperty pProperty() {
            return probability;
        }

        public DoubleProperty aveTProperty() {
            return aveThroughput;
        }

        public DoubleProperty aveDProperty() {
            return aveDelay;
        }
    }

    //number of time slots per value of probability
    private int slot;
    //simulation data list
    private final ObservableList<simulationDataEntry> simuData;
    //the default number of time slots
    public final int TIME_SLOT = 10000;
    //the upper boundry of time slots
    public static final int UPPER_BOUNDRY_TIME_SLOT = 1000000;
    //the lower boundry of time slots
    public static final int LOWER_BOUNDRY_TIME_SLOT = 100;
    //the size of queueing system
    public final int SIZE_QUEUE = 5;
    //probability that a packet will be serviced
    public final double PROBABILITY_DEPARTURE = 0.8;
    //probability that a packet arrives initially
    public final double PROBABILITY_ARRIVAL = 0.02;
    //probability increment
    public final double PROBABILITY_ARRIVAL_INCREMENT = 0.02;

    //constructor
    public DTQSimulation() {
        //default value of time slot
        slot = TIME_SLOT;
        simuData = FXCollections.observableArrayList();
    }

    //accessor method to return the number of slot
    public int getSlot() {
        return slot;
    }

    //mutator method to set the number of slot
    public void setSlot(int timeSlot) {
        slot = timeSlot;
    }

    //mutator method to decrease the number of slot
    public void lessSlot() {
        if (slot <= UPPER_BOUNDRY_TIME_SLOT && slot > 100000) {
            slot -= 100000;
        } else if (slot <= 100000 && slot > 10000) {
            slot -= 10000;
        } else if (slot <= 10000 && slot > 1000) {
            slot -= 1000;
        } else if (slot <= 1000 && slot > 200) {
            slot -= 100;
        } else if (slot <= 200 && slot > LOWER_BOUNDRY_TIME_SLOT) {
            slot = LOWER_BOUNDRY_TIME_SLOT;
        }
    }

    //true if hit,faulse otherwise
    public boolean hitLowerBoundry() {
        return slot <= LOWER_BOUNDRY_TIME_SLOT;
    }

    //mutator method to increase the number of slot
    public void moreSlot() {
        if (slot >= LOWER_BOUNDRY_TIME_SLOT && slot < 1000) {
            slot += 100;
        } else if (slot >= 1000 && slot < 10000) {
            slot += 1000;
        } else if (slot >= 10000 && slot < 100000) {
            slot += 10000;
        } else if (slot >= 100000 && slot < 900000) {
            slot += 100000;
        } else if (slot >= 900000 && slot < UPPER_BOUNDRY_TIME_SLOT) {
            slot = UPPER_BOUNDRY_TIME_SLOT;
        }
    }

    //true if hit,faulse otherwise
    public boolean hitUpperBoundry() {
        return slot >= UPPER_BOUNDRY_TIME_SLOT;
    }

    //accessor method to return the data model that contains the simulation results
    public ObservableList<simulationDataEntry> getSimuData() {
        return simuData;
    }

    //helper method for rounding numbers
    private double round(double number, int decimalPlaces) throws IllegalArgumentException {
        if (decimalPlaces < 0) {
            throw new IllegalArgumentException();
        }
        BigDecimal bd = new BigDecimal(number);
        bd = bd.setScale(decimalPlaces, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    //method to perform the simulation
    public void simulate() {
        //probability that a packet arrives in a slot
        double probability = PROBABILITY_ARRIVAL;
        //total number of serviced packets
        int totalServicedPackets = 0;
        //total number of time slots which all serviced packets spend in time queue
        int totalDelay = 0;
        //average number of serviced packets
        double aveServicedPackets = 0;
        //average number of time slots which a serviced packets spend in time queue
        double aveDelay = 0;
        //data structure to store packets, which also simulates the time queueing system
        TimeQueue timeQ = new TimeQueue();
        //empty the data list every time we run the simulation to avoid data corruption 
        simuData.clear();

        //simulation starts
        //value of probability, starting at 0.02, incrementing by 0.02
        while (probability <= 1.00) {
            //for each value of probability, run the simulation
            for (int i = 0; i < slot; i++) {
                //update time stamp for all packets in queue, if any
                timeQ.updateTimeStamp();

                //departure
                //check if the queue is empty, and a packet will be serviced
                if (!timeQ.isEmpty() && Math.random() <= PROBABILITY_DEPARTURE) {
                    //if true, process and remove the packet in the server(head)
                    totalDelay += timeQ.processPacket();
                    totalServicedPackets++;
                }

                //arrival
                //check if the queue is full, and a packet arrives
                if (timeQ.size() < SIZE_QUEUE && Math.random() <= probability) {
                    timeQ.storePacket();
                }
            }

            //round to 4 decimal places
            try {
                aveServicedPackets = round(((double) totalServicedPackets / slot), 4);
                aveDelay = round(((double) totalDelay / (double) totalServicedPackets), 4);
            } catch (IllegalArgumentException e) {
                System.err.println("IllegalArgumentException: " + e.getMessage());
            }

            //store data
            simuData.add(new simulationDataEntry(probability, aveServicedPackets, aveDelay));

            //reset all total-number counters per value of probability
            totalServicedPackets = 0;
            totalDelay = 0;

            //increase probability, fix floating point issue
            try {
                probability = round((probability += PROBABILITY_ARRIVAL_INCREMENT), 2);
            } catch (IllegalArgumentException e) {
                System.err.println("IllegalArgumentException: " + e.getMessage());
            }
        }//simulation ends
    }
}

