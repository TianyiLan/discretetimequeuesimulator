/*
 * CSE 346 Project #2 Discrete Time Queue
 * Professor: T.G.Robertazzi
 * Programming language: Java
 * Stony Brook University
 * Spring 2016 
 */
package discretetimequeuesimulator;

/**
 * This class serves as a data model to store packets, and provides
 * necessary methods for the simulation. We will simulate a time queueing system
 * with a FIFO basis, and we need to associate a time stamp tag with each
 * packet, thus consider a packet as a node, with time stamp as its data.
 *
 * @author Tianyi Lan
 */
public class TimeQueue {

    //consider a packet as a node
    private class Packet {

        //associated time stamp
        private int timeStamp;
        //reference to the next packet in the queue
        private Packet next;

        //packet constructor
        public Packet() {
            timeStamp = 0;
            next = null;
        }

        //accessor method to return time stamp of a packet
        public int getTimeStamp() {
            return timeStamp;
        }

        //mutator method to increment time stamp of a packet by 1 time slot
        public void incTimeStamp() {
            timeStamp++;
        }
    }

    //define time queue as a linked list
    //the head of time queue is server
    private Packet server;
    private Packet last;
    //the number of packets in the queue
    private int size;

    //timeQueue constructor
    public TimeQueue() {
        server = null;
        size = 0;
    }

    //return true if the time queue is empty
    public boolean isEmpty() {
        return server == null;
    }

    //accessor method to return the number of packets in the queue
    public int size() {
        return size;
    }

    //store a new arrived packet at the end of the queue
    public void storePacket() {
        //new packet with time stamp at 0
        Packet p = new Packet();
        if (isEmpty()) {
            server = p;
        } else {
            last.next = p;
        }
        last = p;
        last.next = null;
        size++;
    }

    //process and remove a packet from the head of the queue, return the delay
    public int processPacket() {
        int delay = 0;
        if (isEmpty()) {
            System.out.println("The queue is empty!");
        } else {
            delay = server.getTimeStamp();
            server = server.next;
            size--;
        }
        return delay;
    }

    //update time stamp for all packets in the queue
    public void updateTimeStamp() {
        Packet current = server;
        while (current != null) {
            current.incTimeStamp();
            current = current.next;
        }
    }
}


