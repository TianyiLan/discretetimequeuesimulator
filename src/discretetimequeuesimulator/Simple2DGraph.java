/*
 * CSE 346 Project #2 Discrete Time Queue
 * Professor: T.G.Robertazzi
 * Programming language: Java
 * Stony Brook University
 * Spring 2016 
 */
package discretetimequeuesimulator;

import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.BorderPane;

/**
 * This class serves as graphic model, and provides necessary methods to plot
 * graphs.
 *
 * @author Tianyi Lan
 */
public class Simple2DGraph {

    private final NumberAxis xAxis;
    private final NumberAxis yAxis;
    private final LineChart graph;
    private final XYChart.Series series;

    //constructor with basic conponents for 2D graph
    //graph reflects as series changes, so add series to graph when constructing
    public Simple2DGraph() {
        xAxis = new NumberAxis();
        yAxis = new NumberAxis();
        graph = new LineChart(xAxis, yAxis);
        series = new XYChart.Series();
        graph.getData().add(series);
    }

    //method to set label for x-axis
    public void setXLabel(String label) {
        xAxis.setLabel(label);
    }

    //method to set label for y-axis
    public void setYLabel(String label) {
        yAxis.setLabel(label);
    }

    //method to set title for the graph
    public void setGraphTitle(String title) {
        graph.setTitle(title);
    }

    //method to set name for the series
    public void setSeriesName(String name) {
        series.setName(name);
    }

    //method to clear data in the series
    public void clearSeries() {
        series.getData().clear();
    }

    //method to insert data to the series
    public void inputData(double x, double y) {
        series.getData().add(new XYChart.Data(x, y));
    }

    //method to return the graph
    public LineChart getGraph() {
        return graph;
    }

    //method to show point entries
    public void showPoints(boolean show) {
        graph.setCreateSymbols(show);
    }

    //method to plot the graph
    //for this program, the graph can be only displayed in the center of a borderpane 
    public void plot(BorderPane pane) {
        pane.setCenter(graph);
    }
}


